package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ActivityProfiles extends ActivityRoot{

    private Activity mContext;
    private Class_User _User;
    private Class_Profiles _Profiles;
    private Class_Locations _Locations;
    private Location location = null;
    private double latitude = 0;
    private double longitude = 0;
    private int distance = 20;
    private JSONObject Profiles = new JSONObject();

    @Override
    public int getLayoutResource() {
        return R.layout.activity_profiles;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_maps);
        mContext = this;

        _User = ((App) getApplicationContext()).User();
        _Profiles = ((App)getApplicationContext()).Profiles();
        _Locations = ((App)getApplicationContext()).Locations();
        location = _Locations.location;
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        distance = _Locations.distance;

        //CONFIG
        new Class_Options(this);
        ClassDataStorage.AsyncResponse CallbackSettings = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("distance")){
                        distance = Integer.parseInt(Callback.getString("distance"));
                    }

                    _Locations.distance= distance;
                    ConfigDistance();

                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        new ClassDataStorage(mContext, "read", "settings", "", CallbackSettings).execute("");
    }

    public void ConfigDistance(){
        /*
        final String DistanceUnit = getString(R.string.ActivitySettingsDistanceUnit);
        final TextView TextViewDistance = (TextView) findViewById(R.id.TextDistance);
        SeekBar seekBarDistance = (SeekBar ) findViewById(R.id.distance);
        seekBarDistance.setProgress(distance);
        TextViewDistance.setText(String.valueOf(distance)+" "+DistanceUnit);
        seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                TextViewDistance.setText(String.valueOf(distance)+" "+DistanceUnit);
                distance = progresValue;
                progress = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                TextViewDistance.setText(String.valueOf(distance)+" "+DistanceUnit);
                SetDistance(distance);
                GetPositionMap();
            }
        });
        */

        //CREATEMAPS
        UpdatePositionMap();
    }

    private void ProfilesSearch(){
        final Runnable ProfilesCallback = new Runnable() {
            @Override
            public void run() {
                Profiles =_Profiles.Profiles;
                showProfiles();

                if(Profiles.length()>0){

                }else{
                    //new ClassGeneral().CreateMenssage(mContext,"Places", String.valueOf(mContext.getText(R.string.MessagePlacesNotFound)));

                }
            }
        };
        _Profiles.GetProfiles(mContext, ProfilesCallback);
    }

    private void showProfiles(){
        GridView gridView = (GridView) findViewById(R.id.GridViewProfiles);
        ClassGridviewProfiles gridViewAdapter = new ClassGridviewProfiles(this, Profiles);

        gridView.setAdapter(gridViewAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CharSequence idProfile = ((TextView) view.findViewById(R.id.gridview_id)).getText();
                CharSequence name = ((TextView) view.findViewById(R.id.gridview_text)).getText();
                //Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, ActivityProfile.class);
                intent.putExtra("option",idProfile);
                mContext.startActivity(intent);
            }
        });

    }

    public void GetPositionMap(){
        location = _Locations.location;
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        ProfilesSearch();
    }


    public void UpdatePositionMap(){
        _Locations.GetLocation(mContext, new Runnable() {
            public void run() {
                GetPositionMap();
            }
        });

        if(latitude==0 && longitude==0){

        }
    }

    private void UpdateStorage(){
        try {
            JSONObject user = new JSONObject();
            user.put("latitude", String.valueOf(_Locations.latitude));
            user.put("longitude", String.valueOf(_Locations.longitude));
            user.put("distance", String.valueOf(_Locations.distance));
            ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                @Override
                public void processFinish(String output){
                    //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                    //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                }
            };

            new ClassDataStorage(mContext, "write", "locations", user.toString(), Callback).execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}