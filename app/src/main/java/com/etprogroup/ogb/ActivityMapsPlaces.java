package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ActivityMapsPlaces extends ActivityRoot implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener  {
    private Activity mContext;
    private JSONObject Places = new JSONObject();
    private Map<MarkerOptions, String> markerIds = new HashMap<>();
    private GoogleMap mMap;
    private Class_User _User;
    private Class_Places _Places;

    //LOCATIONS
    private Class_Locations _Locations;
    private double latitude = 0;
    private double longitude = 0;
    private int distance;
    private Marker LocationMarker = null;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_mapsplaces;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapsplaces);
        mContext = this;

        //HEADER SETPS
        new Class_Options(this, "locations");

        _User = ((App) getApplicationContext()).User();
        _Places = ((App) getApplicationContext()).Places();
        _Locations = ((App) getApplicationContext()).Locations();
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        distance = _Locations.distance;

        //CONFIG
        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("latitude")){
                    }

                    if(!Callback.isNull("longitude")){
                    }

                    if(!Callback.isNull("distance")){
                        _Locations.distance= Integer.parseInt(Callback.getString("distance"));
                    }

                    //PLACES
                    PlaceSearch();

                }catch(JSONException e) {
                    e.printStackTrace();
                }

            }
        };
        new ClassDataStorage(mContext, "read", "locations", "", Callback).execute("");


        final TextView TextViewDistance = (TextView) findViewById(R.id.TextDistance);
        SeekBar seekBarDistance = (SeekBar ) findViewById(R.id.distance);
        seekBarDistance.setProgress(distance);
        TextViewDistance.setText(String.valueOf(distance)+" "+getString(R.string.ActivityMapDistance));
        seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                TextViewDistance.setText(String.valueOf(distance)+" "+getString(R.string.ActivityMapDistance));
                distance = progresValue;
                progress = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                TextViewDistance.setText(String.valueOf(distance)+" "+getString(R.string.ActivityMapDistance));
                SetDistance(distance);
                UpdateStorage();
                PlaceSearch();
            }
        });

    }

    private void PlaceSearch(){
        final Runnable PlacesCallback = new Runnable() {
            @Override
            public void run() {
                Places=_Places.Places;
                if(Places.length()>0){
                    CreateMap();

                }else{
                    new ClassGeneral().CreateMenssage(mContext,"Places", String.valueOf(mContext.getText(R.string.MessagePlacesNotFound)));

                }
            }
        };

        _Places.GetMarkerPlacesServer(mContext, _User, PlacesCallback);
    }

    public void SetDistance(int distance){
        _Locations.distance = distance;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(19.0f);//19
        mMap.setMapStyle(_Locations.GetMapStyle());
        mMap.setInfoWindowAdapter(_Locations.CustomInfoWindowAdapter(mContext));

        if(LocationMarker!=null){
            LocationMarker.remove();
        }

        //User
        LatLng location = new LatLng(latitude, longitude);
        MarkerOptions Marker = _Locations.CreateMarker(location,mContext.getString(R.string.ActivityMapLocation));
        LocationMarker = mMap.addMarker(Marker);
        //LocationMarker.showInfoWindow();

        LatLngBounds.Builder BuilderBounds = new LatLngBounds.Builder();
        BuilderBounds.include(Marker.getPosition());

        //Places
        Iterator<String> iter = Places.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                JSONObject place = Places.getJSONObject(key);
                double Platitude = Double.parseDouble(place.getString("latitude"));
                double Plongitude = Double.parseDouble(place.getString("longitude"));
                String PTitle = place.getString("title");

                location = new LatLng(Platitude, Plongitude);
                Marker = _Locations.CreateMarkerPlaces(location, key, PTitle);

                markerIds.put(Marker, key);
                LocationMarker = mMap.addMarker(Marker);
                //LocationMarker.showInfoWindow();
                BuilderBounds.include(Marker.getPosition());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.40);

        LatLngBounds bounds = BuilderBounds.build();
        mMap.setLatLngBoundsForCameraTarget(bounds);
        //mMap.setOnInfoWindowClickListener(this);

        //CameraUpdate Camera = CameraUpdateFactory.newLatLng(location);
        CameraUpdate Camera = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        mMap.animateCamera(Camera);
        //mMap.moveCamera(Camera);
    }


    public void CreateMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        _Locations.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void onInfoWindowClick(Marker marker) {
        onMarkerClick(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.hideInfoWindow();
        //String id = markerIds.get(marker);
        String id = marker.getSnippet();
        //Log.d("onMarkerClick",String.valueOf(id));
        if(id!="" && id!=null){
            _Places.PlaceSelect = id;
            Intent intent = new Intent(this, ActivityPlace.class);
            intent.putExtra("Place_SELECT",id);
            startActivity(intent);
        }
        return false;
    }

    private void UpdateStorage(){
        try {
            JSONObject user = new JSONObject();
            user.put("latitude", String.valueOf(_Locations.latitude));
            user.put("longitude", String.valueOf(_Locations.longitude));
            user.put("distance", String.valueOf(_Locations.distance));
            ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                @Override
                public void processFinish(String output){
                    //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                    //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                }
            };

            new ClassDataStorage(mContext, "write", "locations", user.toString(), Callback).execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}