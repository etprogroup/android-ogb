package com.etprogroup.ogb;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

/**
 * Created by Bronco on 30-07-2017.
 */

public class App extends MultiDexApplication {
    public Application mContext;
    public Class_User _User;
    public Class_Locations _Locations;
    public Class_Services _Services;
    public Class_Profiles _Profiles;
    public Class_Places _Places;

    public App(){
        mContext = this;
        _User = new Class_User(mContext);
        _Locations = new Class_Locations(mContext);
        _Services = new Class_Services(mContext);
        _Places = new Class_Places(mContext);
        _Profiles = new Class_Profiles(mContext);
    }

    //USERDATA
    public Class_User User(){
        return _User;
    }

    //USERMAPS
    public Class_Locations Locations(){
        return _Locations;
    }

    //SERVICES
    public Class_Services Services(){
        return _Services;
    }

    //PROFILES
    public Class_Profiles Profiles(){
        return _Profiles;
    }

    //PLACES
    public Class_Places Places(){
        return _Places;
    }

}
