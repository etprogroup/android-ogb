package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class ActivityChat extends ActivityRoot{

    private Activity mContext;
    private Class_User _User;
    private String chat = "";
    private String members = "{}";
    private JSONObject messages = new JSONObject();
    private ListView listView = null;
    private ClassListViewChat adapter = null;
    private EditText EditMessage;
    private int countPrint = 0;
    private android.os.Handler handler;
    private Runnable runnableSetInterval;

    @Override
    public int getLayoutResource() {
        return R.layout.activity_chat;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_chat);
        mContext = this;
        _User = ((App) getApplicationContext()).User();
        handler = new android.os.Handler();


        //DATA
        Intent data = getIntent();
        chat = data.getStringExtra("option");
        members = data.getStringExtra("members");

        //MESSAGES
        CreateListViewChat();

        //SEND
        ImageView iconSend;
        iconSend = (ImageView) findViewById(R.id.iconSend);
        iconSend.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SendMessage();
            }
        });

        //Tolbar
        if(toolbar != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        fillChatRunnableOut();
    }

    private void CreateListViewChat() {
        try {
            messages = new JSONObject("{}");
            listView =(ListView)findViewById(R.id.ListViewMessages);
            adapter= new ClassListViewChat(this, messages, _User.GetDataToken());

            listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CharSequence textID = ((TextView) view.findViewById(R.id.textID)).getText();
                    CharSequence textMessage = ((TextView) view.findViewById(R.id.textMessage)).getText();

                    Snackbar.make(view, textMessage+" \n "+textID, Snackbar.LENGTH_LONG)
                            .setAction("No action", null).show();
                }
            });

            //MESSAGES
            fillChat();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillChatRunnable(){
        runnableSetInterval = new Runnable() {
            public void run() {
                try {
                    fillChat();

                } catch (NoSuchMethodError e) {
                    fillChatRunnableOut();
                }
            }
        };

        handler.postDelayed(runnableSetInterval,10000);
    }

    private void fillChatRunnableOut(){
        handler.removeCallbacks(runnableSetInterval);
    }

    private void fillChat(){
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "chat");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("option", "get");
            data.put("members", members);

        } catch (JSONException e){
            e.printStackTrace();
        }

        ClassDataServer.AsyncResponse UserResponse = new ClassDataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new ClassGeneral().CreateMenssage(mContext,"Update", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new ClassGeneral().CreateMenssage(mContext,"Update", message);

                        if(!response.isNull("chat")){
                            chat = response.getString("chat");
                            messages = new JSONObject(response.getString("messages"));
                            showMessages();

                            countPrint++;
                            if(countPrint<=1){
                                scrollListViewToBottom();
                            }

                            //SetInterval
                            fillChatRunnable();
                        }

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);
                    }

                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };
        new ClassDataServer(mContext, data, UserResponse, false).execute("");
    }


    private void showMessages(){
        //new ClassGeneral().CreateMenssage(mContext,"messages", messages.toString());
        //adapter.notifyDataSetChanged();
        adapter.updateData(messages);
    }


    //SEND MESSAGE
    public void SendMessage() {
        EditMessage = (EditText) findViewById(R.id.EditMessage);

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "chat");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("option", "send");
            data.put("members", members);
            data.put("message", EditMessage.getText().toString());
            data.put("chat", chat);

        } catch (JSONException e){
            e.printStackTrace();
        }


        ClassDataServer.AsyncResponse UserResponse = new ClassDataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new ClassGeneral().CreateMenssage(mContext,"Update", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new ClassGeneral().CreateMenssage(mContext,"Update", message);
                        EditMessage.setText("");
                        //fillChat();

                        chat = response.getString("chat");
                        messages = new JSONObject(response.getString("messages"));
                        scrollListViewToBottom();
                        showMessages();


                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);
                    }

                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };
        new ClassDataServer(mContext, data, UserResponse, false).execute("");
    }

    private void scrollListViewToBottom(){

        listView.post(new Runnable() {
            @Override
            public void run() {
                //Select the last row so it will scroll into view...
                listView.setSelection(adapter.getCount() - 1);
                Log.i("scrollListView adapter", String.valueOf((adapter.getCount() - 1)));
            }
        });
    }

}