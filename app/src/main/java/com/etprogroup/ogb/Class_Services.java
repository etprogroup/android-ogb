package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Bronco on 20-07-2017.
 */

public class Class_Services{
    public JSONObject services;
    private Context mContext;

    public Class_Services(Context context) {
        services = new JSONObject();
        //services = Create_services();
        mContext = context;
    }


    public JSONObject Create_services(){
        JSONObject service = new JSONObject();

        try{
            for(int i = 0; i < 4 ; i++){
                String id = "service"+i;
                service.put("id", "");
                service.put("link", id);
                service.put("title", id);
                service.put("content", "Content "+id);
                service.put("detail", "Detail "+id);
                service.put("price", "price "+id);
                service.put("duration", "duration "+id);
                services.put(id,service);

            }

        }catch(JSONException e){
            e.printStackTrace();
        }

        return services;
    }

    public JSONObject GetServices(){
        return services;
    }

    public JSONObject GetServicesRedirect(Activity context){

        if(services.length() == 0){
            Intent Intent = new Intent(context, ActivityMain.class);
            context.startActivityForResult(Intent, 0);

        }

        return services;
    }

    public String GET_Data(String name, String type){
        String data = "";
        try {
            JSONObject service = services.getJSONObject(name);
            data = service.getString(type);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    public String GET_Data_ByTitle(String title, String type){
        String data = "";

        Iterator<String> iter = services.keys();
        while(iter.hasNext()) {
            String key = iter.next();
            String name = this.GET_Title(key);
            if(title.equals(name)){
                data = GET_Data(key, type);
                break;
            }
        }

        return data;
    }

    public String GET_Title(String name){
        return this.GET_Data(name,"title");
    }

    public JSONObject Get_ServerServices(final Activity context, final Runnable Callback){
        JSONObject data = new JSONObject();
        try {
            data.put("action", "services");
            data.put("option", "get");

        } catch (JSONException e){
            e.printStackTrace();
        }

        ClassDataServer.AsyncResponse response = new ClassDataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new ClassGeneral().CreateMenssage(context,"output", output);
                //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_SHORT).show();
                Log.d("output",output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new ClassGeneral().CreateMenssage(context,"Exito", message);
                        services = new JSONObject(response.getString("services"));
                        Callback.run();

                    }else if(result.equals("error")){
                        new ClassGeneral().CreateMenssage(context,"Error", message);
                        //Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new ClassDataServer(context, data, response).execute("");
        return services;

    }


}
