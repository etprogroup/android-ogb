package com.etprogroup.ogb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

/**
 * Created by Bronco on 10-09-2017.
 */

public class ClassDataServer extends AsyncTask<String, Void, String> {
    public Context mContext;
    public String upload = "";
    public String Authorization = "";
    public static final int CONNECTION_TIMEOUT = 15 * 1000;
    public ProgressDialog mProgressDialog = null;
    public Boolean dialog = true;
    public AsyncResponse delegate = null;
    public JSONObject data;

    public ClassDataServer(Context context) {
        this.mContext = context;
    }

    public ClassDataServer(Context context, JSONObject JsonData, AsyncResponse delegate) {
        this.mContext = context;
        this.delegate = delegate;
        this.data = JsonData;
        this.upload = mContext.getString(R.string.URLServer)+"api/upload.php";

        String auth = mContext.getString(R.string.AppAuthorizationUser)+":"+mContext.getString(R.string.AppAuthorizationPass);
        this.Authorization = "Basic " +Base64.encodeToString(auth.getBytes(),Base64.DEFAULT);
    }

    public ClassDataServer(Context context, JSONObject JsonData, AsyncResponse delegate, Boolean dialog) {
        this.mContext = context;
        this.delegate = delegate;
        this.data = JsonData;
        this.upload = mContext.getString(R.string.URLServer)+"api/upload.php";

        String auth = mContext.getString(R.string.AppAuthorizationUser)+":"+mContext.getString(R.string.AppAuthorizationPass);
        this.Authorization = "Basic " +Base64.encodeToString(auth.getBytes(),Base64.DEFAULT);
        this.dialog = dialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(mContext != null && mContext instanceof Activity && dialog){
            mProgressDialog = new ProgressDialog(mContext, R.style.CustomStyleProgressDialog);
            //mProgressDialog.setTitle("Conected");
            //mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

        }
    }


    protected String doInBackground(String... params) {
        String JsonResponse = null;
        String JsonDATA = params[0];
        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            String StringURL = Build_URLGet(upload, data);
            String StringData = getPostDataString(data);
            //String StringData = data.toString();
            //Log.d("LOGStringData", StringData);
            //Log.d("LOGStringData", StringURL);

            URL url = new URL(upload);
            //URL url = new URL(StringURL);

            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(CONNECTION_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);

            connection.setRequestMethod("POST");
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);

            connection.addRequestProperty("Authorization", Authorization);
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            //connection.getOutputStream().write(data.toString().getBytes("UTF-8"));
            connection.connect();


            OutputStream out = new BufferedOutputStream(connection.getOutputStream());
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
            writer.write(data.toString());//StringData
            writer.flush();
            writer.close();
            out.close();


            InputStream inputStream = connection.getInputStream();
            if (inputStream == null) {
                return null;
            }

            reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer buffer = new StringBuffer();

            String inputLine;
            while ((inputLine = reader.readLine()) != null){
                buffer.append(inputLine + "\n");
            }

            if (buffer.length() == 0) {
                return null;
            }

            JsonResponse = buffer.toString();
            return JsonResponse;

        } catch (IOException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (connection  != null) {
                connection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {

                }
            }
        }
        return null;
    }

    protected void onPostExecute(String result) {
        if(result==null){
            result="";
        }
        delegate.processFinish(result);

        if(mProgressDialog != null){
            mProgressDialog.dismiss();

        }
    }

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public String Build_URLGet(String url, JSONObject params) {
        try {
            return url+"?"+getPostDataString(params);

        } catch (Exception e) {
            e.printStackTrace();

        }

        return url;
    }

    public String getPostDataString(JSONObject params) throws Exception {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> itr = params.keys();

        while(itr.hasNext()){

            String key= itr.next();
            Object value = params.get(key);

            if(first){
                first = false;

            }else{
                result.append("&");
            }

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }

}
