package com.etprogroup.ogb;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by Carlos Gomez on 07-11-2017.
 */

public class ServiceFirebaseMessagingService extends FirebaseMessagingService {
    public String TAGLog = "TESTFirebase";
    public int IntMessageCount = 0;
    public String body = null;
    public String title = null;
    public String action = null;
    public String tag = null;
    public String classname = null;
    public String option = null;
    public String messageCount = null;
    public Map<String, String> data = null;

    private String user = "";
    private String token = "";
    public Boolean sound = false;
    public Boolean vibration = true;
    public Boolean lights = true;
    public Boolean notify = true;
    public String channel = "app";
    public int DefaultOptions = Notification.DEFAULT_LIGHTS;
    private String TAG = "FireBaseService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAGLog, "remoteMessage: " + String.valueOf(remoteMessage));
        Log.e(TAGLog, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.e(TAGLog, "Message data payload: " + remoteMessage.getData());
            data = remoteMessage.getData();
            classname = data.get("classname");
            messageCount = data.get("messageCount");
            option = data.get("option");

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAGLog, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            body = remoteMessage.getNotification().getBody();
            title = remoteMessage.getNotification().getTitle();
            action = remoteMessage.getNotification().getClickAction();
            tag = remoteMessage.getNotification().getTag();
        }

        //sendNotification();
        storageSettings();
    }



    private void sendNotification() {
        Log.e(TAGLog, "sendNotification: ");

        int IntMessageCount = 0;
        if(messageCount!=null){
            if(messageCount!=""){
                IntMessageCount = Integer.parseInt(messageCount);
            }
        }

        Class ClassAction = ActivityMain.class;
        Intent resultIntent = new Intent(this, ClassAction);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        if(classname!=null){
            if(classname!=""){
                resultIntent.putExtra("classname", classname);
            }
        }


        if(option!=null){
            if(option!=""){
                resultIntent.putExtra("option", option);
            }
        }

        if(tag!=null){
            if(tag!=""){
                resultIntent.putExtra("tag", tag);
            }
        }

        if(data!=null){
            resultIntent.putExtra("data", String.valueOf(data));
        }

        //OPTIONS
        if(sound && vibration && lights){
            DefaultOptions = Notification.DEFAULT_ALL;
            Log.e(TAG, "sound && vibration && lights:"+DefaultOptions);

        }else if(!sound && !vibration){
            DefaultOptions = Notification.DEFAULT_LIGHTS;
            Log.e(TAG, "!sound && !vibration:"+DefaultOptions);

        }else if(!sound){
            DefaultOptions = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
            Log.e(TAG, "!sound:"+DefaultOptions);

        }else if(!vibration){
            DefaultOptions = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND;
            Log.e(TAG, "!vibration:"+DefaultOptions);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0 ,
                resultIntent,
                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);//, channel
        notificationBuilder.setSmallIcon(R.drawable.launcher)
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setColor(Color.WHITE)
                            .setSound(defaultSoundUri)
                            .setNumber(IntMessageCount)
                            .setContentIntent(pendingIntent)
                            .setDefaults(DefaultOptions);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0 , notificationBuilder.build());

        /*
        //notification badges
        String id = getString(R.string.ActivityNotifyChannel);;
        CharSequence name = getString(R.string.ActivityNotifyChannelName);
        String description = getString(R.string.ActivityNotifyChannelDesc);
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);

        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        notificationManager.createNotificationChannel(mChannel);
        */
    }




    private void storageSettings() {
        // TODO: Implement this method to send token to your app server.
        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                Log.e(TAG, "StorageSttings:"+output);

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("sound")){
                        sound = Boolean.parseBoolean(Callback.getString("sound"));
                    }

                    if(!Callback.isNull("vibration")){
                        vibration = Boolean.parseBoolean(Callback.getString("vibration"));
                    }

                    if(!Callback.isNull("notify")){
                        notify = Boolean.parseBoolean(Callback.getString("notify"));
                    }

                    if(notify){
                        sendNotification();
                    }

                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new ClassDataStorage(this, "read", "settings", "", Callback).execute("");
    }
}
