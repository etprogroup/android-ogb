package com.etprogroup.ogb;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Carlos Gomez on 13-11-2017.
 */

public class Class_Profiles {
    Activity ActContext;
    Context mContext;
    public JSONObject Profiles = new JSONObject();
    public Class_User _User;
    public Class_Locations _Locations;
    public double latitude = 0;
    public double longitude = 0;
    public int distance = 5;
    final int Style = R.style.CustomStyleAlertDialog;

    public Class_Profiles() {
    }

    public Class_Profiles(Application mContext) {
    }


    public void GetProfiles(final Activity context, final Runnable callback) {
        _User = ((App)context.getApplicationContext()).User();
        _Locations =  ((App)context.getApplicationContext()).Locations();
        latitude = _Locations.latitude;
        longitude = _Locations.longitude;
        distance = _Locations.distance;

        //Place DATA
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "profiles");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("latitude", String.valueOf(latitude));
            data.put("longitude", String.valueOf(longitude));
            data.put("radio", String.valueOf(distance));

        } catch (JSONException e){
            e.printStackTrace();
        }

        ClassDataServer.AsyncResponse response = new ClassDataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {

                try {
                    JSONObject response = new JSONObject(output);
                    String title = response.getString("title");
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new ClassGeneral().CreateMenssage(context,title, message);
                        //Log.d("Places",response.getString("profiles"));
                        Profiles = response.getJSONObject("profiles");
                        callback.run();

                    }else if(result.equals("notfound")){
                        Profiles = new JSONObject();
                        callback.run();

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(context, ActivityMain.class);
                        context.startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(context,title, message);
                    }

                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(context,"Profiles", output);
                    e.printStackTrace();
                }

            }
        };

        new ClassDataServer(context, data, response).execute("");
    }

    public JSONObject GetProfilesFake() {
        int factor = 1;
        for (int id = 1; id < 10; id++) {
            try {
                JSONObject Profile = new JSONObject();
                Profile.put("id", id);
                Profile.put("image", "");
                Profile.put("name", "Title" + id);
                Profile.put("latitude", latitude + (2 * id * factor / 10));
                Profile.put("longitude", longitude + (2 * id * factor / 10));
                Profiles.put(String.valueOf(id), Profile);
                factor = factor * (-1);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return Profiles;
    }


    public void ServiceProfiles(final Context context, final String outputLocations) {
        Log.e("ServicePlaces", "ServicePlaces:"+outputLocations);
        //new ClassGeneral().CreateMenssage(context,"Places", output);


        // TODO: Implement this method to send token to your app server.
        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String outputSettings){
                Log.e("ServicePlaces", "ServicePlacesSettings:"+outputSettings);
                Boolean sound = true;
                Boolean vibration = true;
                Boolean notify = true;

                try {
                    JSONObject Callback = new JSONObject(outputSettings);
                    if(!Callback.isNull("sound")){
                        sound = Boolean.parseBoolean(Callback.getString("sound"));
                    }

                    if(!Callback.isNull("vibration")){
                        vibration = Boolean.parseBoolean(Callback.getString("vibration"));
                    }

                    if(!Callback.isNull("notify")){
                        notify = Boolean.parseBoolean(Callback.getString("notify"));
                    }


                    if(notify){

                        //PLACES
                        JSONObject response = new JSONObject(outputLocations);
                        String result = response.getString("result");
                        String message = response.getString("message");

                        if(result.equals("success")){
                            Profiles = response.getJSONObject("places");

                            int iPlace = 0;
                            Iterator<String> iter = Profiles.keys();
                            while (iter.hasNext()) {
                                String key = iter.next();

                                JSONObject place = Profiles.getJSONObject(key);
                                String id = place.getString("id");
                                String title = place.getString("title");
                                String content = place.getString("content");

                                //NNOTIFY
                                ClassNotify _Notify = new ClassNotify();
                                _Notify.IntMessage = iPlace;
                                //_Notify.IntMessageCount = (iPlace+1);
                                _Notify.classname = ".ActivityPlace";
                                _Notify.option = id;
                                _Notify.vibration = vibration;
                                _Notify.sound = sound;
                                _Notify.notify = notify;
                                _Notify.Notify(context, title, content);
                                iPlace++;
                            }
                        }

                    }


                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new ClassDataStorage(context, "read", "settings", "", Callback).execute("");
    }

}
