package com.etprogroup.ogb;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.widget.ImageView;

import java.util.ArrayList;

import javax.security.auth.callback.Callback;

import static android.widget.ImageView.ScaleType.CENTER_CROP;

/**
 * Created by Bronco on 18-07-2017.
 */

public class ClassGeneral {
    public static Context mContext;
    public static Activity actContext;
    final int Style = R.style.CustomStyleAlertDialog;

    public ClassGeneral() {
    }

    public ClassGeneral(Context context) {
        mContext = context;
    }

    public void CreateMenssage(Context context, String title, String message){
        //Dialog dialog = new Dialog(mContext);
        //dialog.setContentView(R.layout.activity_userupdate);
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage(message);
        dialog.setTitle(title);
        dialog.show();
    }

    public String CreateURL(Context context, String url){
        String NewPath = "";
        if(!url.equals("")){
            String path = context.getString(R.string.URLServer);
            NewPath =path+url;
        }
        return NewPath;
    }

    public void SetImageDefault(ImageView Image) {
        SetImageDefault(Image, R.drawable.users);
    }

    public void SetImageDefault(ImageView Image, int Drawable) {
        //Image.setBackgroundColor(Color.WHITE);
        Image.setBackgroundResource(Drawable);
        //Image.setImageResource(R.drawable.ic_persona);
        Image.setScaleType(CENTER_CROP);
    }

    //REDIRECT ACTIVITY
    public void ActivityRedirect(Class activity){
        ActivityRedirect(ActivityMain.class, activity);
    }

    public void ActivityRedirect(Class activity, Class activityreturn){
        ActivityRedirect(activity, activityreturn, null);
    }

    public void ActivityRedirect(Class activity, Class activityreturn, String tag){
        Intent Intent = new Intent(mContext, activity);
        Intent.putExtra("classname", activityreturn);
        Intent.putExtra("tag", tag);
        mContext.startActivity(Intent);
    }

    //NOTIFY
    public void ActivityNotify(String title, String message, String action, String actiontext){
        Intent Intent = new Intent(mContext, ActivityNotification.class);
        Intent.putExtra("title", title);
        Intent.putExtra("message", message);
        Intent.putExtra("action", action);
        Intent.putExtra("actiontext", actiontext);
        mContext.startActivity(Intent);
    }



    public void AlertDialog(String title, String message) {
        Runnable Callback = new Runnable() {
            @Override
            public void run() {

            }
        };
        AlertDialog(title, message, Callback);
    }



    public void AlertDialog(String title, String message, final Runnable Callback) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext, Style);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setNegativeButton(mContext.getString(R.string.AlertDialogNegative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Callback.run();
            }
        });

        builder.show();
    }

    //HTML
    public static Spanned fromHtml(String html){
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html,Html.FROM_HTML_MODE_COMPACT);//FROM_HTML_MODE_LEGACY
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public int ColorResources(Context context, int color){
        return ResourcesCompat.getColor(context.getResources(), color, null);
    }


    /**///GET PATH BY URI
    public ArrayList<String> getRealPathFromURI(final Context context, Uri contentURI) {
        //contentURI = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        ArrayList<String> list = new ArrayList<String>();

        String [] columns={
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.TITLE,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATE_ADDED};

        Cursor cursor = mContext.getContentResolver().query(contentURI, columns, null, null, null);
        new ClassGeneral().CreateMenssage(mContext,"cursor Count", String.valueOf(cursor.getCount()));

        if (cursor == null) {
            // Source is Dropbox or other similar local file path
            list.add(contentURI.getPath());

        } else if(cursor.getCount() > 0){

            if(cursor.moveToFirst()) {
                int photoIdIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
                int photoPathIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                int photoNameIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
                int photoTitleIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.TITLE);
                int photoSizeIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE);
                int bucketDisplayNameIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                int bucketIdIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_ID);
                int photoAddDate = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATE_ADDED);
                int totalNum = cursor.getCount();

                do {
                    String id = cursor.getString(photoIdIndex);
                    String name = cursor.getString(photoNameIndex);
                    String path = cursor.getString(photoPathIndex);
                    String title = cursor.getString(photoTitleIndex);
                    String size = cursor.getString(photoSizeIndex);
                    String bucketId = cursor.getString(bucketIdIndex);
                    String bucketname = cursor.getString(bucketDisplayNameIndex);
                    String addDate = cursor.getString(photoAddDate);
                    list.add(path);

                } while (cursor.moveToNext());
            }
        }

        cursor.close();
        new ClassGeneral().CreateMenssage(mContext,"result", String.valueOf(list));
        return list;
    }

    /**///GET PATH BY URI
    public static int GENERAL_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 1;
    public static Uri mUri;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPathbyURI(final Activity context, final Uri uri) {
        actContext = context;
        mUri = uri;
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentPlace
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStoragePlace
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsPlace
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaPlace
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Activity context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {


            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        context,
                        new String[]{ android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        GENERAL_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                return null;
            }else{
                cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);

            }
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }



    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {//GENERAL_PERMISSIONS_WRITE_EXTERNAL_STORAGE
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getPathbyURI(actContext, mUri);

                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return;
            }
        }
    }
}