package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 08-11-2017.
 */

public class ActivityGeneralSettings extends ActivityRoot {
    private Activity mContext;
    private Class_User _User;
    private LinearLayout Content;
    private View Separator;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_general;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);
        mContext = this;

        _User = ((App)getApplicationContext()).User();
        _User.UserCheck(mContext, ActivityGeneralSettings.class);

        //ICON
        ImageView Icon = (ImageView) findViewById(R.id.imageDetail);
        Icon.setImageResource(R.drawable.ic_settings);
        Icon.setVisibility(View.VISIBLE);

        //TITLE
        TextView TextTitle = (TextView) findViewById(R.id.TextDetail);
        TextTitle.setText(getString(R.string.ActivityGeneralSettings));

        //CONTENT
        Content = (LinearLayout) mContext.findViewById(R.id.LayoutContent);
        Content.setPadding(50,50,50,50);

        //ACTION
        UserAction();
        Content.addView(ViewSeparate());
        PlaceAction();
        Content.addView(ViewSeparate());
        LogOutAction();
        Content.addView(ViewSeparate());
        ExitAction();
        Content.addView(ViewSeparate());

    }


    private void LogOutAction() {

        final TextView TextAction = new TextView(mContext);
        TextAction.setTextColor(Color.WHITE);
        TextAction.setTextSize(20);
        TextAction.setPadding(20,40,20,40);
        TextAction.setText(getString(R.string.ActivityGeneralSettingsLogout));
        Content.addView(TextAction);


        TextAction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                _User.Check = false;
                _User.SetDataToken("");
                _User.SetDataUser("");
                _User.SetDataImage("");
                _User.SetDataData("{}");

                try {
                    JSONObject user = new JSONObject();
                    user.put("token", "");
                    user.put("user", "");
                    user.put("data", "{}");
                    user.put("places", "{}");

                    ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                        @Override
                        public void processFinish(String output){
                            Intent Intent = new Intent(mContext, ActivityMain.class);
                            Intent.putExtra("option", "back");
                            startActivity(Intent);
                        }
                    };

                    new ClassDataStorage(mContext, "write", "user", user.toString(), Callback).execute("");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void UserAction() {
        final TextView TextAction = new TextView(mContext);
        TextAction.setTextColor(Color.WHITE);
        TextAction.setTextSize(20);
        TextAction.setPadding(20,40,20,40);
        TextAction.setText(getString(R.string.ActivityGeneralSettingsProfile));
        Content.addView(TextAction);

        TextAction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent Intent = new Intent(mContext, ActivityUserUpdate.class);
                startActivity(Intent);
            }
        });
    }

    private void PlaceAction() {
        final TextView TextAction = new TextView(mContext);
        TextAction.setTextColor(Color.WHITE);
        TextAction.setTextSize(20);
        TextAction.setPadding(20,40,20,40);
        TextAction.setText(getString(R.string.ActivityGeneralSettingsPlace));
        Content.addView(TextAction);

        TextAction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                /*
                Intent Intent = new Intent(mContext, ActivityProfilePlace.class);
                startActivity(Intent);*/
            }
        });
    }

    private void ExitAction() {
        final TextView TextAction = new TextView(mContext);
        TextAction.setTextColor(Color.WHITE);
        TextAction.setTextSize(20);
        TextAction.setPadding(20,40,20,40);
        TextAction.setText(getString(R.string.ActivityGeneralSettingsExit));
        Content.addView(TextAction);

        TextAction.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    private View ViewSeparate(){
        final TextView Separate = new TextView(mContext);
        Separate.setBackgroundColor(Color.BLACK);
        Separate.setTextSize(0);
        Separate.setPadding(0,3,0,0);
        return Separate;
    }

}
