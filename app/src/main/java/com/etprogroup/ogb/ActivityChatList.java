package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

public class ActivityChatList extends ActivityRoot{

    private Activity mContext;
    private Class_User _User;
    private String chat = "";
    private String members = "{}";
    private JSONObject messages = new JSONObject();

    @Override
    public int getLayoutResource() {
        return R.layout.activity_chat;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_chat);
        mContext = this;
        _User = ((App) getApplicationContext()).User();

        //DATA
        Intent data = getIntent();
        chat = data.getStringExtra("option");
        members = data.getStringExtra("members");

        //IMAGES
        fillChat();

        //Tolbar
        if(toolbar != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void fillChat(){
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "chat");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("members", members);

        } catch (JSONException e){
            e.printStackTrace();
        }


        ClassDataServer.AsyncResponse UserResponse = new ClassDataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new ClassGeneral().CreateMenssage(mContext,"Update", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new ClassGeneral().CreateMenssage(mContext,"Update", message);

                        if(!response.isNull("chat")){
                            chat = response.getString("chat");
                            messages = new JSONObject(response.getString("messages"));
                            showMessage();
                        }

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);
                    }

                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };
        new ClassDataServer(mContext, data, UserResponse).execute("");
    }


    private void showMessage(){
        new ClassGeneral().CreateMenssage(mContext,"messages", messages.toString());

    }

}