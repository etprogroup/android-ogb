package com.etprogroup.ogb;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityUserSigninCode extends ActivityRoot {
    private Context mContext;
    private EditText userCode;
    private Class_User _User;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_usersignincode;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usersignincode);
        mContext = this;

        _User = ((App)getApplicationContext()).User();
        userCode = (EditText) findViewById(R.id.EditCode);

        //Button
        Button ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Validate();
            }

        });

    }


    public void Validate(){
        JSONObject data = new JSONObject();
        try {
            data.put("action", "user_validate");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("code", userCode.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }

        ClassDataServer.AsyncResponse response = new ClassDataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_SHORT).show();
                //new ClassGeneral().CreateMenssage(mContext,"response", output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");
                    final String title = response.getString("title");

                    if(result.equals("success")){
                        _User.SetDataToken(response.getString("token"));
                        _User.SetDataUser(response.getString("user"));
                        _User.SetDataData(response.getString("data"));
                        //new ClassGeneral().CreateMenssage(mContext,title, output);

                        JSONObject user = new JSONObject();
                        user.put("token", _User.GetDataToken());
                        user.put("user", _User.GetDataUser());
                        user.put("data", _User.GetDataData());

                        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                            @Override
                            public void processFinish(String output){
                                //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                                //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                            }
                        };

                        new ClassDataStorage(mContext, "write", "user", user.toString(), Callback).execute("");

                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivityForResult(Intent, 0);

                    }else if(result.equals("error")){
                        new ClassGeneral().CreateMenssage(mContext,title, message);
                        //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        new ClassDataServer(mContext, data, response).execute("");
    }

}
