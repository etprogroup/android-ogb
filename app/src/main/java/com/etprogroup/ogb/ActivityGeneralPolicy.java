package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 08-11-2017.
 */

public class ActivityGeneralPolicy extends ActivityRoot {
    public Activity mContext;
    private Class_User _User;
    private String Title;
    private JSONObject Page;
    private TextView TextTitle;
    private int RESULT = RESULT_CANCELED;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_general;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);
        mContext = this;

        //TOOLBAR
        _User = ((App)getApplicationContext()).User();
        if(_User.Check){
        }


        //ICON
        ImageView Icon = (ImageView) findViewById(R.id.imageDetail);
        Icon.setImageResource(R.drawable.ic_ayuda);
        Icon.setVisibility(View.VISIBLE);

        //TITLE
        TextTitle = (TextView) findViewById(R.id.TextDetail);
        TextTitle.setText(getString(R.string.ActivityDetail));

        ClassDataServer.AsyncResponse response = new ClassDataServer.AsyncResponse(){

            @Override
            public void processFinish(String output){
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //Title  = response.getString("title");
                        String SPage = response.getString("page");
                        Page = new JSONObject(SPage);
                        if(Page.length()>0){
                            PageAction();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };


        try {
            //POLICY
            final JSONObject data = new JSONObject();
            data.put("action", "pages");
            data.put("option", "policy");
            new ClassDataServer(mContext, data, response).execute("");

        } catch (JSONException e){
            e.printStackTrace();
        }
    }


    private void PageAction() {
        LinearLayout General = (LinearLayout) mContext.findViewById(R.id.ActivityGeneral);
        LinearLayout Content = (LinearLayout) mContext.findViewById(R.id.LayoutContent);
        Content.setPadding(50,50,50,50);

        try {
            String title = Page.getString("title");
            String content = Page.getString("content");

            //TITLE
            TextTitle.setText(title);

            //CONTENT
            final LinearLayout BuilderLayout = new LinearLayout(mContext);
            BuilderLayout.setOrientation(LinearLayout.VERTICAL);
            BuilderLayout.setPadding(20,40,20,30);

            final TextView TextContent = new TextView(mContext);
            TextContent.setTextColor(Color.WHITE);
            TextContent.setText(content);
            TextContent.setText(new ClassGeneral(mContext).fromHtml(content));
            TextContent.setTextSize(12);
            BuilderLayout.addView(TextContent);

            final WebView WebViewContent = new WebView(mContext);
            WebViewContent.loadData(content, "text/html", "utf-8");
            //BuilderLayout.addView(WebViewContent);


            //ADD LAYOUT
            Content.addView(BuilderLayout);

        } catch (JSONException e) {
            e.printStackTrace();
        }



        //BUTTONS
        final LinearLayout BuilderLayoutButton = new LinearLayout(mContext);
        BuilderLayoutButton.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams LayoutButtonParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        BuilderLayoutButton.setLayoutParams(LayoutButtonParams);

        LinearLayout.LayoutParams ButtonParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1.0f);
        ButtonParams.setMargins(20,20,20,20);

        final Button ButtonNegative = (Button) getLayoutInflater().inflate(R.layout.view_button, null);
        ButtonNegative.setTextAppearance(mContext, R.style.ThemeButton);
        ButtonNegative.setText(getString(R.string.ActivityDetailNegative));
        ButtonNegative.setLayoutParams(ButtonParams);
        BuilderLayoutButton.addView(ButtonNegative);

        final Button ButtonPositive = (Button) getLayoutInflater().inflate(R.layout.view_button, null);
        ButtonPositive.setTextAppearance(mContext, R.style.ThemeButton);
        ButtonPositive.setText(getString(R.string.ActivityDetailPositive));
        ButtonPositive.setLayoutParams(ButtonParams);
        BuilderLayoutButton.addView(ButtonPositive);

        ButtonPositive.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                RESULT = RESULT_OK;
                onBackPressed();
            }
        });

        ButtonNegative.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v){
                RESULT = RESULT_CANCELED;
                onBackPressed();
            }
        });

        //ADD BUTTONS
        General.addView(BuilderLayoutButton);
    }


    public void onBackPressed(){
        Intent intent = new Intent();
        setResult(RESULT, intent);
        finish();
    }

}
