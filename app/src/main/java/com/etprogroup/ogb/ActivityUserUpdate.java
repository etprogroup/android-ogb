package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityUserUpdate extends ActivityRoot {
    public Activity mContext;
    public Class_User _User;

    TextView TextProfile;
    EditText EditImage;
    EditText EditName;
    EditText EditLastName;
    EditText EditContent;
    EditText EditDate;
    EditText EditEmail;
    EditText EditPhone;
    EditText EditPass;
    EditText EditPassConfirm;
    ImageView ProfileImage;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_userupdate;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_userupdate);
        mContext = this;

        //USER
        _User = ((App)getApplicationContext()).User();
        String data = String.valueOf(_User.GetDataData());
        String user = _User.GetDataUser();
        String image = _User.GetDataImage();
        String fname = _User.GetData("fname", "value");
        String lname = _User.GetData("lname", "value");
        String content = _User.GetData("content", "value");
        String phone = _User.GetData("phone", "value");
        String date = _User.GetData("date", "value");
        FillNameProfile();

        //UPLOAD IMAGE
        ProfileImage = (ImageView) findViewById(R.id.ProfileImage);
        new ClassGeneral().SetImageDefault(ProfileImage);
        String URL = new ClassGeneral().CreateURL(mContext,image);
        new ClassImageDownload(mContext, ProfileImage).execute(URL);
        ProfileImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UploadImage();
            }
        });

        EditImage = (EditText) findViewById(R.id.EditImage);
        EditImage.setText(image);

        EditName = (EditText) findViewById(R.id.EditName);
        EditName.setText(fname);

        EditLastName = (EditText) findViewById(R.id.EditLastName);
        EditLastName.setText(lname);

        EditContent = (EditText) findViewById(R.id.EditContent);
        EditContent.setText(content);

        EditDate = (EditText) findViewById(R.id.EditDate);
        EditDate.setText(date);

        EditEmail = (EditText) findViewById(R.id.EditEmail);
        EditEmail.setText(user);

        EditPhone = (EditText) findViewById(R.id.EditPhone);
        EditPhone.setText(phone);

        EditPass = (EditText) findViewById(R.id.EditPass);
        EditPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        EditPassConfirm = (EditText) findViewById(R.id.EditPassConfirm);
        EditPassConfirm.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        //FABS
        configureFabUpdate();
        configureFabAddImage();

        //Button Data
        Button ButtonApply;
        ButtonApply = (Button) findViewById(R.id.ButtonApply);
        ButtonApply.setEnabled(true);
        ButtonApply.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                UploadInfo();
            }

        });
    }

    public void FillNameProfile(){
        String name = BuildName(getString(R.string.ActivityProfileData));
        TextProfile = (TextView) findViewById(R.id.TextProfile);
        TextProfile.setText(name);
    }


    //UPLOAD INFO
    public void UploadInfo() {

        final JSONObject data = new JSONObject();
        try {
            data.put("action", "user_update");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("fname", EditName.getText().toString());
            data.put("lname", EditLastName.getText().toString());
            data.put("content", EditContent.getText().toString());
            data.put("phone", EditPhone.getText().toString());
            data.put("date", EditDate.getText().toString());
            data.put("pass", EditPass.getText().toString());
            data.put("passc", EditPassConfirm.getText().toString());
            data.put("image", EditImage.getText().toString());

        } catch (JSONException e){
            e.printStackTrace();
        }


        ClassDataServer.AsyncResponse UserResponse = new ClassDataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new ClassGeneral().CreateMenssage(mContext,"Update", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        new ClassGeneral().CreateMenssage(mContext,"Update", message);

                        if(!response.isNull("data")){
                            _User.SetDataData(response.getString("data"));
                            FillNameProfile();
                            FillNameDrawer();
                            FillImageDrawer();
                        }

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);
                    }

                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };
        new ClassDataServer(mContext, data, UserResponse).execute("");

    }

    //FABS
    private void configureFabUpdate() {
        //Floating Action Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.FABupdate);
        if(fab != null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {UploadInfo();
                }
            });
        }
    }

    private void configureFabAddImage() {
        //Floating Action Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.FABaddImage);
        if(fab != null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RedirectAction(ActivityUserImages.class);
                }
            });
        }
    }



    //UPLOAD IMAGE
    private ClassDataServerFile DataServerFile;
    private static final int CAPTURE_IMAGE_ACTIVITY = 1;
    private Uri mImageCaptureUri;

    public void UploadImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CAPTURE_IMAGE_ACTIVITY);
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY) {
            //TODO: action
            if (data == null) {
                //Display an error
                return;
            }

            Uri URIimage = data.getData();
            String Image = new ClassGeneral(mContext).getPathbyURI(mContext, URIimage);

            if(Image != null){
                File FileImage = new File(Image);

                /*
                ArrayList<String>  ListImage = new ClassGeneral(mContext).getRealPathFromURI(mContext, URIimage);
                String Image = ListImage.get(0);
                Log.d("ListImage", String.valueOf(ListImage));
                Log.d("Image",  String.valueOf(Image));
                */

                if(FileImage.exists()){
                    UploadImages(Image, URIimage);

                }else{
                    new ClassGeneral().CreateMenssage(mContext,"Error", "Error Image Selected");

                }
            }

        }
    }



    public void UploadImages(final String path, final Uri URIimage){
        final JSONObject data = new JSONObject();
            try {
            data.put("action", "image");
            data.put("token", _User.GetDataToken());
            data.put("user", _User.GetDataUser());
            data.put("path", "uploads/profile/");
            data.put("name_change", true);
            data.put("image_name", "file");
            data.put("image", path);

        } catch (JSONException e){
            e.printStackTrace();
        }



        final ClassDataServerFile.AsyncResponse FileResponse = new ClassDataServerFile.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new ClassGeneral().CreateMenssage(mContext,"output", output);
                //Log.d("output",output);

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("true")){
                        final String file = response.getString("file");
                        _User.SetDataImage(file);
                        ClassImageDownload.Cache.getInstance().getLru().remove(new ClassGeneral().CreateURL(mContext,file));
                        ProfileImage.setImageURI(URIimage);
                        EditImage.setText(file);
                        UploadInfo();

                    }else if(result.equals("false")){
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);

                    }else if(result.equals("success")){

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Result", output);

                    }


                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };

        DataServerFile = new ClassDataServerFile(mContext, data, path, FileResponse);
        DataServerFile.execute("");
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(DataServerFile != null){
            DataServerFile.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}

