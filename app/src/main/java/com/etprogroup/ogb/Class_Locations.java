package com.etprogroup.ogb;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static android.location.LocationManager.GPS_PROVIDER;


/**
 * Created by Bronco on 03-08-2017.
 */

public class Class_Locations implements com.google.android.gms.location.LocationListener {

    Activity ActContext;
    Context mContext;

    public boolean isGPSEnabled = false;
    public boolean isNetworkEnabled = false;
    public boolean canGetLocation = false;

    public Location location;
    public double latitude = 0;
    public double longitude = 0;

    public Object placename;
    public int distance = 5;
    public JSONObject Address = new JSONObject();
    public String AddressSelect = "";

    private static final int PERMISSIONS_ACCESS_LOCATION = 1;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; //10;
    private static final long MIN_TIME_BW_UPDATES = 0; //1000 * 60 * 1;
    protected LocationManager LManager;
    private static Runnable ActCallback;

    private android.location.LocationListener mlocationListener = new android.location.LocationListener() {
        public void onLocationChanged(Location location) {
            //Any method here
        }
        public void onStatusChanged (String provider, int status, Bundle extras){}
        public void onProviderEnabled(String provider) {}
        public void onProviderDisabled (String provider){}
    };


    public Class_Locations(Context context) {
        mContext = context;
        //GetLocation(ActContext);
    }

    public double GetLatitude() {
        return latitude;
    }

    public void SetLatitude(double lat) {
        latitude = lat;
    }

    public double GetLongitude() {
        return longitude;
    }

    public void SetLongitude(double lng) {
        longitude = lng;
    }

    public double GetGPSLatitude() {
        latitude = location.getLatitude();
        return latitude;
    }

    public double GetGPSLongitude() {
        longitude = location.getLongitude();
        return longitude;
    }

    public MapStyleOptions GetMapStyle() {
        MapStyleOptions MapStyle;
        //MapStyle = new MapStyleOptions(mContext.getResources().getString(R.string.google_maps_style));
        MapStyle = MapStyleOptions.loadRawResourceStyle(mContext, R.raw.google_maps_style);
        return MapStyle;
    }

    public void SetPlace(Place place) {
        LatLng Location = place.getLatLng();
        latitude = Location.latitude;
        longitude = Location.longitude;
        placename = place.getName();
        Address = GetAddress();
    }

    public void SetMarker(Marker marker) {
        LatLng Location = marker.getPosition();
        latitude = Location.latitude;
        longitude = Location.longitude;
        Address = GetAddress();
    }

    public JSONObject GetAddress() {
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        List<android.location.Address> addresses;

        try {
            if (Geocoder.isPresent()) {
                // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String address = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();

                Address.put("address", address);
                Address.put("city", city);
                Address.put("state", state);
                Address.put("country", country);
                Address.put("postalCode", postalCode);
                //Address.put("knownName", knownName);
            }
        } catch (IOException e) {
            e.printStackTrace();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Address;
    }


    public Location GetLocation(Activity context, Runnable Callback) {
        ActContext = context;
        ActCallback = Callback;

        try {
            LManager = (LocationManager) ActContext.getSystemService(ActContext.LOCATION_SERVICE);
            isGPSEnabled = LManager.isProviderEnabled(GPS_PROVIDER);
            isNetworkEnabled = LManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (ActivityCompat.checkSelfPermission(ActContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(ActContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
/*
                if (ActivityCompat.shouldShowRequestPermissionRationale(ActContext,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
                }else{

                }
*/
                ActivityCompat.requestPermissions(ActContext,
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION},
                        PERMISSIONS_ACCESS_LOCATION);

            } else if (isGPSEnabled) {
                this.canGetLocation = true;
                LManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES,
                        mlocationListener);

                if (LManager != null) {
                    location = LManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    /**/location = GetLastKnownLocations(ActContext);

                    if (location == null) {
                        location = GetLastKnownLocations(ActContext);
                    }

                    if (location != null) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        Address = GetAddress();
                        ActCallback.run();
                        return location;
                    }
                }

            }else if (isNetworkEnabled) {
                    this.canGetLocation = true;
                    LManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES,
                            mlocationListener);

                    if (LManager != null) {
                        location = LManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        /**/location = GetLastKnownLocations(ActContext);

                        if (location == null) {
                            location = GetLastKnownLocations(ActContext);
                        }

                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            Address = GetAddress();
                            ActCallback.run();
                            return location;
                        }
                    }

            } else {
                Log.d("no isGPSEnabled", String.valueOf(isGPSEnabled));
                Log.d("no isNetworkEnabled", String.valueOf(isNetworkEnabled));
                new ClassGeneral().CreateMenssage(ActContext,"Error", "GPS and Network Disable");

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    public Location GetLastKnownLocations(Context context) {
        LocationManager mLocationManager;
        mLocationManager = (LocationManager) context.getSystemService(ActContext.LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;

        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return bestLocation;
            }

            Location location = mLocationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }

            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = location;
                //break;
            }
        }

        return bestLocation;
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_ACCESS_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GetLocation(ActContext, ActCallback);

                } else {
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
                return;
            }
        }
    }

    public void onLocationChanged(Location location) {
        //Log.d("no onLocationChanged class", String.valueOf(location));
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }


    //MARKER
    public MarkerOptions CreateMarker(LatLng location, String title){
        //BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.map_marker_location_new);

        //String title = getCompleteAddressString(location.latitude, location.longitude);
        //String title = "Location";

        MarkerOptions Marker = new MarkerOptions();
        Marker.position(location);
        Marker.title(title);
        Marker.draggable(true);
        Marker.icon(icon);
        return Marker;
    }

    public MarkerOptions CreateMarkerPlaces(LatLng location, String id, String title){
        //BitmapDescriptor icon = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.map_marker_new);

        MarkerOptions Marker = new MarkerOptions();
        Marker.position(location);
        Marker.title(title);
        Marker.snippet(id);
        //Marker.draggable(true);
        Marker.icon(icon);
        return Marker;
    }



    public CustomInfoWindowAdapter CustomInfoWindowAdapter(Activity context){
        return new CustomInfoWindowAdapter(context);
    }

    public static class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View ContentsView;

        CustomInfoWindowAdapter(Activity context){
            ContentsView = context.getLayoutInflater().inflate(R.layout.view_maps_infowindow, null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView Title = ((TextView) ContentsView.findViewById(R.id.title));
            Title.setText(marker.getTitle());

            TextView Snippet = ((TextView) ContentsView.findViewById(R.id.snippet));
            Snippet.setText(marker.getSnippet());
            Snippet.setVisibility(View.GONE);

            return ContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }
}
