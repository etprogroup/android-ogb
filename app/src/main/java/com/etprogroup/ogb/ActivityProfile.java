package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Bronco on 15-07-2017.
 */

public class ActivityProfile extends ActivityRoot {
    private Activity mContext;
    private Class_User _User;
    private Class_Locations _Locations;
    private Class_Profiles _Profiles;

    private JSONObject data = new JSONObject();
    private JSONObject Profiles = new JSONObject();
    private String Profile = "";
    private String ProfileSelect = null;
    private String ProfileSelectAddress = "";
    private ImageView imageProfile;
    private ImageView imageProfileFull;
    private JSONObject Images = new JSONObject();

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_profile);
        mContext = this;

        //DATA
        Intent data = getIntent();
        ProfileSelect = data.getStringExtra("option");

        //IMAGES
        imageProfileFull = (ImageView) findViewById(R.id.imageProfileFull);
        imageProfile = (ImageView) findViewById(R.id.imageProfile);
        new ClassGeneral().SetImageDefault(imageProfile);//, R.drawable.services

        _User = ((App)getApplicationContext()).User();
        _Locations = ((App)getApplicationContext()).Locations();

        //Profiles
        _Profiles = ((App)getApplicationContext()).Profiles();
        Profiles = _Profiles.Profiles;

        //ProfileSelect = _Profiles.ProfileSelect;
        if(ProfileSelect!="" && ProfileSelect!=null){
            FillProfile(ProfileSelect);
        }


        //BUTTON BACK
        Button ButtonBack = (Button) findViewById(R.id.ButtonBack);
        ButtonBack.setEnabled(true);
        ButtonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                onBackPressed();
            }
        });

        //Tolbar
        if(toolbar != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        //FABS
        configureFabChat();
        configureFabViewImages();

    }

    //@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void FillProfile(String id){
        try {
            JSONObject Profile = Profiles.getJSONObject(id);

            String URLimageProfile = Profile.getString("image");
            String URLProfile = new ClassGeneral().CreateURL(mContext,URLimageProfile);
            new ClassImageDownload(mContext, imageProfile).execute(URLProfile);
            //new ClassImageDownload(mContext, imageProfileFull).execute(URLProfile);

            TextView ProfileName = (TextView) mContext.findViewById(R.id.ProfileName);
            ProfileName.setText(Profile.getString("name"));

            TextView ProfileContent = (TextView) mContext.findViewById(R.id.ProfileContent);
            ProfileContent.setText(Profile.getString("content"));

            //Data
            LinearLayout LayoutData = (LinearLayout) mContext.findViewById(R.id.LayoutProfileData);
            LayoutData.setPadding(0,50,0,50);


            String ProfileImages = Profile.getString("images");
            JSONObject JSONProfileImages = new JSONObject(ProfileImages);
            if(JSONProfileImages.length()<=0){
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.FABviewImages);
                fab.setVisibility(View.GONE);
            }


            //BLUR
            LinearLayout LayoutimageProfileFull = (LinearLayout) mContext.findViewById(R.id.LayoutimageProfileFull);
            Bitmap BitmapImageProfileFull = (Bitmap) ClassImageDownload.Cache.getInstance().getLru().get(URLProfile);
            if (BitmapImageProfileFull != null) {
                ClassViewBlur BlurBuilder = new ClassViewBlur();
                Bitmap blurredBitmap = BlurBuilder.blur(this, BitmapImageProfileFull);
                //LayoutimageProfileFull.setBackgroundDrawable( new BitmapDrawable( getResources(), blurredBitmap ) );

                //Bitmap bitmap = BitmapFactory.decodeResource(getResources(), blurredBitmap);
                imageProfileFull.setImageBitmap(blurredBitmap);
                imageProfileFull.setVisibility(View.VISIBLE);
            }


            try {
                //data = new JSONObject(Profile.getString("offers"));
                data = Profile.getJSONObject("data");
                Iterator<String> iter = data.keys();
                while(iter.hasNext()) {
                    String key = iter.next();

                    JSONObject detail = data.getJSONObject(key);
                    String title = detail.getString("title");
                    String content = detail.getString("content");

                    if(!content.equals("")){
                        final LinearLayout BuilderLayout = new LinearLayout(mContext);
                        BuilderLayout.setOrientation(LinearLayout.VERTICAL);
                        BuilderLayout.setPadding(0,2,0,0);

                        //TITLE
                        final TextView TextTitle = new TextView(mContext);
                        TextTitle.setTextColor(Color.WHITE);
                        TextTitle.setTextSize(20);
                        TextTitle.setText(title);
                        TextTitle.setPadding(50,20,50,10);
                        BuilderLayout.addView(TextTitle);

                        //CONTENT
                        final TextView TextContent = new TextView(mContext);
                        //TextContent.setBackgroundColor(ResourcesCompat.getColor(mContext.getResources(), R.color.colorTransparent, null));
                        //TextContent.setBackgroundColor(Color.BLACK);
                        TextContent.setTextColor(Color.WHITE);
                        //TextContent.setVisibility(View.GONE);
                        TextContent.setText(content);
                        //TextContent.setText(new ClassGeneral(mContext).fromHtml(content));
                        TextContent.setTextSize(18);
                        TextContent.setPadding(50,10,50,50);
                        BuilderLayout.addView(TextContent);

                        //ADD LAYOUT
                        LayoutData.addView(BuilderLayout);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    //FABS
    private void configureFabChat() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.FABchat);
        if(fab != null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                try {
                    JSONObject members = new JSONObject();
                    members.put("1",_User.GetDataToken());
                    members.put("2",ProfileSelect);

                    Intent intent = new Intent(mContext, ActivityChat.class);
                    intent.putExtra("option", "");
                    intent.putExtra("members", members.toString());
                    mContext.startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                }
            });
        }
    }

    private void configureFabViewImages() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.FABviewImages);
        if(fab != null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, ActivityProfileImages.class);
                    intent.putExtra("option", ProfileSelect);
                    mContext.startActivity(intent);
                }
            });
        }
    }

}
