package com.etprogroup.ogb;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Created by Bronco on 24-07-2017.
 */

public class ClassGridviewCustom extends BaseAdapter {

    private Context mContext;
    private final int gridViewLayout;
    private final int gridViewOrientation;
    private final String[] gridViewString;
    private final int[] gridViewImage;

    public ClassGridviewCustom(Context context, String[] gridViewString, int[] gridViewImage) {
        mContext = context;
        this.gridViewImage =  gridViewImage;
        this.gridViewString = gridViewString;
        this.gridViewLayout = R.layout.gridview_custom;
        this.gridViewOrientation = LinearLayout.VERTICAL;
    }

    public ClassGridviewCustom(Context context, String[] gridViewString, int[] gridViewImage, int gridViewLayout) {
        mContext = context;
        this.gridViewImage =  gridViewImage;
        this.gridViewString = gridViewString;
        this.gridViewLayout = gridViewLayout;
        this.gridViewOrientation = LinearLayout.VERTICAL;
    }

    public ClassGridviewCustom(Context context, String[] gridViewString, int[] gridViewImage, int gridViewLayout, int gridViewOrientation) {
        mContext = context;
        this.gridViewImage = gridViewImage;
        this.gridViewString = gridViewString;
        this.gridViewLayout = gridViewLayout;
        this.gridViewOrientation = gridViewOrientation;
    }

    @Override
    public int getCount() {
        return gridViewString.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridView;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            gridView = new View(mContext);
            gridView = inflater.inflate(gridViewLayout, null);

            LinearLayout layoutView = (LinearLayout) gridView.findViewById(R.id.gridview_layout);
            TextView textView = (TextView) gridView.findViewById(R.id.gridview_text);
            ImageView imageView = (ImageView) gridView.findViewById(R.id.gridview_image);
            layoutView.setOrientation(gridViewOrientation);

            if(gridViewString[i]!=""){
                textView.setText(gridViewString[i]);
                textView.setVisibility(View.VISIBLE);
            }else{
                textView.setVisibility(View.GONE);
            }

            if(gridViewImage[i]!=0){
                imageView.setImageResource(gridViewImage[i]);
                imageView.setVisibility(View.VISIBLE);
            }else{
                imageView.setVisibility(View.GONE);
            }

        } else {
            gridView = convertView;
        }

        return gridView;
    }


    public int GetColor(int color){
        return ResourcesCompat.getColor(mContext.getResources(), color, null);
    }


    //SET COLOR
    public void Runnable_FillColorAll(final GridView Grid, final int color){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                FillColorAll(Grid, color);
            }
        });
    }


    public void FillColorAll(GridView Grid, int color){
        final int size = Grid.getChildCount();
        for(int i = 0; i < size; i++) {
            this.FillColorItem(Grid, color, i);
        }
    }


    public void Runnable_FillColorItem(final GridView Grid, final int color, final int item){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                FillColorItem(Grid, color, item);
            }
        });
    }

    public void FillColorItem(GridView Grid, int color, int item){
        ViewGroup gridChild = (ViewGroup) Grid.getChildAt(item);
        int childSize = gridChild.getChildCount();
        for(int k = 0; k < childSize; k++) {

            //TEXT
            if( gridChild.getChildAt(k) instanceof TextView ) {
                TextView textView = (TextView) gridChild.getChildAt(k);
                textView.setTextColor(color);

            //ICON
            }else if(gridChild.getChildAt(k) instanceof ImageView){
                ImageView imageView = (ImageView)  gridChild.getChildAt(k);
                imageView.setColorFilter(color);
            }
        }
    }

    //SET PADDING
    public void Runnable_PaddingAll(final GridView Grid, final int padding_left, final int padding_top, final int padding_right, final int padding_bottom){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                PaddingAll(Grid, padding_left, padding_top, padding_right, padding_bottom);
            }
        });
    }


    public void PaddingAll(GridView Grid, final int padding_left, final int padding_top, final int padding_right, final int padding_bottom){
        final int size = Grid.getChildCount();
        for(int i = 0; i < size; i++) {
            this.PaddingItem(Grid, i, padding_left, padding_top, padding_right, padding_bottom);
        }
    }

    public void Runnable_PaddingItem(final GridView Grid, final int item, final int padding_left, final int padding_top, final int padding_right, final int padding_bottom){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                PaddingItem(Grid, item, padding_left, padding_top, padding_right, padding_bottom);
            }
        });
    }

    public void PaddingItem(GridView Grid, int item, int padding_left, int padding_top, int padding_right, int padding_bottom){
        ViewGroup gridChild = (ViewGroup) Grid.getChildAt(item);
        int childSize = gridChild.getChildCount();
        for(int k = 0; k < childSize; k++) {
            LinearLayout.LayoutParams  linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);

            //TEXT
            if( gridChild.getChildAt(k) instanceof TextView ) {
                TextView textView = (TextView) gridChild.getChildAt(k);
                textView.setLayoutParams(linearLayoutParams);
                textView.setPadding(padding_left, padding_top, padding_right, padding_bottom);

                //ICON
            }else if(gridChild.getChildAt(k) instanceof ImageView){
                ImageView imageView = (ImageView)  gridChild.getChildAt(k);
                imageView.setLayoutParams(linearLayoutParams);
                imageView.setPadding(padding_left, padding_top, padding_right, padding_bottom);
            }
        }
    }


    //SET FONT SIZE
    public void Runnable_FontSizeItem(final GridView Grid, final int item, final int FontSize){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                FontSizeItem(Grid, item, FontSize);
            }
        });
    }

    public void FontSizeItem(GridView Grid, int item, int FontSize){
        ViewGroup gridChild = (ViewGroup) Grid.getChildAt(item);
        int childSize = gridChild.getChildCount();
        for(int k = 0; k < childSize; k++) {

            //TEXT
            if( gridChild.getChildAt(k) instanceof TextView ) {
                TextView textView = (TextView) gridChild.getChildAt(k);
                textView.setTextSize(FontSize);
            }
        }
    }




    //SET GRAVITY
    public void Runnable_GravityAll(final GridView Grid, final int Gravity){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                GravityAll(Grid, Gravity);
            }
        });
    }


    public void GravityAll(GridView Grid, final int Gravity){
        final int size = Grid.getChildCount();
        for(int i = 0; i < size; i++) {
            this.GravityItem(Grid, i, Gravity);
        }
    }

    public void Runnable_GravityItem(final GridView Grid, final int item, final int Gravity){
        Grid.post(new Runnable() {
            @Override
            public void run() {
                GravityItem(Grid, item, Gravity);
            }
        });
    }

    public void GravityItem(GridView Grid, int item, int Gravity){
        ViewGroup gridChild = (ViewGroup) Grid.getChildAt(item);
        int childSize = gridChild.getChildCount();
        for(int k = 0; k < childSize; k++) {
            LinearLayout.LayoutParams  linearLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.FILL_PARENT);

            //TEXT
            if( gridChild.getChildAt(k) instanceof TextView ) {
                TextView textView = (TextView) gridChild.getChildAt(k);

                linearLayoutParams.gravity = Gravity;
                textView.setLayoutParams(linearLayoutParams);

                textView.setGravity(Gravity);
                textView.measure(View.MeasureSpec.makeMeasureSpec( textView.getMeasuredWidth(), View.MeasureSpec.EXACTLY),
                        View.MeasureSpec.makeMeasureSpec(textView.getMeasuredHeight() , View.MeasureSpec.AT_MOST));
            }
        }
    }

}