package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ActivityMaps extends ActivityRoot implements
        OnMapReadyCallback,
        GoogleMap.OnMarkerDragListener,
        LocationListener,
        GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMarkerClickListener{

    private Activity mContext;
    private GoogleMap mMap;
    private JSONObject Address = new JSONObject();
    private TextView TextViewAddress;
    private Class_User _User;
    private Class_Places _Places;
    private Class_Locations _Locations;
    private Location location = null;
    private double latitude = 0;
    private double longitude = 0;
    private int distance = 20;
    private JSONObject Places = new JSONObject();
    private Map<MarkerOptions, String> markerIds = new HashMap<>();
    private String AddressSelect = "";
    private Marker LocationMarker;
    private Marker LocationMarkerPlaces;

    @Override
    public int getLayoutResource() {
        return R.layout.activity_maps;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_maps);
        mContext = this;

        _User = ((App) getApplicationContext()).User();
        _Places = ((App) getApplicationContext()).Places();
        _Locations = ((App)getApplicationContext()).Locations();
        location = _Locations.location;
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        distance = _Locations.distance;

        //MAP AUTOCOMPLETE
        PlaceAutocompleteFragment placeAutocomplete = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.placeAutocomplete);

        EditText place_autocomplete_search_input = (EditText)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_search_input);
        place_autocomplete_search_input.setTextSize(15);
        place_autocomplete_search_input.setTextColor(Color.WHITE);
        place_autocomplete_search_input.setHintTextColor(Color.WHITE);

        ImageButton place_autocomplete_search_button = (ImageButton)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_search_button);
        place_autocomplete_search_button.setImageResource(R.drawable.ic_place);
        place_autocomplete_search_button.setColorFilter(Color.WHITE);

        ImageButton place_autocomplete_clear_button = (ImageButton)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_clear_button);
        place_autocomplete_clear_button.setColorFilter(Color.WHITE);

        ImageView place_autocomplete_powered_by_google = (ImageView)placeAutocomplete.getView().findViewById(R.id.place_autocomplete_powered_by_google);
        //place_autocomplete_powered_by_google.setVisibility(View.GONE);

        placeAutocomplete.setHint(getResources().getString(R.string.ActivityMapSearch));
        if(AddressSelect!=""){
            placeAutocomplete.setText(AddressSelect);
        }

        try {
            placeAutocomplete.setOnPlaceSelectedListener(new PlaceSelectionListener() {
                @Override
                public void onPlaceSelected(Place place) {
                    PlaceResult(place);
                }

                @Override
                public void onError(Status status) {
                    Log.d("Place error", "An error occurred: " + status);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }



        //CONFIG
        ClassDataStorage.AsyncResponse CallbackSettings = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("distance")){
                        distance = Integer.parseInt(Callback.getString("distance"));
                    }

                    _Locations.distance= distance;
                    ConfigDistance();

                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        };
        new ClassDataStorage(mContext, "read", "settings", "", CallbackSettings).execute("");
    }

    public void ConfigDistance(){
        final String DistanceUnit = getString(R.string.ActivitySettingsDistanceUnit);
        final TextView TextViewDistance = (TextView) findViewById(R.id.TextDistance);
        SeekBar seekBarDistance = (SeekBar ) findViewById(R.id.distance);
        seekBarDistance.setProgress(distance);
        TextViewDistance.setText(String.valueOf(distance)+" "+DistanceUnit);
        seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                TextViewDistance.setText(String.valueOf(distance)+" "+DistanceUnit);
                distance = progresValue;
                progress = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                TextViewDistance.setText(String.valueOf(distance)+" "+DistanceUnit);
                SetDistance(distance);
                GetPositionMap();
            }
        });



        //CREATEMAPS
        UpdatePositionMap();
    }

    private void PlaceSearch(){
        final Runnable PlacesCallback = new Runnable() {
            @Override
            public void run() {
                Places =_Places.Places;
                CreateMap();

                if(Places.length()>0){

                }else{
                    //new ClassGeneral().CreateMenssage(mContext,"Places", String.valueOf(mContext.getText(R.string.MessagePlacesNotFound)));

                }
            }
        };

        _Places.GetMarkerPlacesServer(mContext, _User, PlacesCallback);
    }

    public void GetPositionMap(){
        location = _Locations.location;
        latitude = _Locations.GetLatitude();
        longitude = _Locations.GetLongitude();
        PlaceSearch();
    }


    public void UpdatePositionMap(){
        _Locations.GetLocation(mContext, new Runnable() {
            public void run() {
                GetPositionMap();
            }
        });

        if(latitude==0 && longitude==0){

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();
        mMap.setMinZoomPreference(6.0f);
        mMap.setMaxZoomPreference(19.0f);
        mMap.setMapStyle(_Locations.GetMapStyle());
        mMap.setInfoWindowAdapter(_Locations.CustomInfoWindowAdapter(mContext));
        mMap.setOnMarkerDragListener(this);

        /*//CONTROLLER
        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);
        //UiSettings.setMapToolbarEnabled(true);
        //UiSettings.setCompassEnabled(true);
        */

        if(LocationMarker!=null){
            LocationMarker.remove();
        }

        LatLng location = new LatLng(latitude, longitude);
        MarkerOptions Marker = _Locations.CreateMarker(location,mContext.getString(R.string.ActivityMapLocation));
        LocationMarker = mMap.addMarker(Marker);
        //LocationMarker.showInfoWindow();

        LatLngBounds.Builder BuilderBounds = new LatLngBounds.Builder();
        BuilderBounds.include(Marker.getPosition());

        //Places
        Iterator<String> iter = Places.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                JSONObject place = Places.getJSONObject(key);
                double Platitude = Double.parseDouble(place.getString("latitude"));
                double Plongitude = Double.parseDouble(place.getString("longitude"));
                String PTitle = place.getString("title");

                location = new LatLng(Platitude, Plongitude);
                Marker = _Locations.CreateMarkerPlaces(location, key, PTitle);

                markerIds.put(Marker, key);
                LocationMarkerPlaces = mMap.addMarker(Marker);
                //LocationMarker.showInfoWindow();
                BuilderBounds.include(Marker.getPosition());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        final int width = getResources().getDisplayMetrics().widthPixels;
        final int height = getResources().getDisplayMetrics().heightPixels;
        final int minMetric = Math.min(width, height);
        final int padding = (int) (minMetric * 0.40);

        LatLngBounds bounds = BuilderBounds.build();
        //mMap.setLatLngBoundsForCameraTarget(bounds);
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            public void onInfoWindowClick(Marker marker){
                MapsInfoOptionRedirect(marker);
            }
        });

        //CameraUpdate Camera = CameraUpdateFactory.newLatLng(location);
        CameraUpdate Camera = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        //mMap.animateCamera(Camera);
        mMap.moveCamera(Camera);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                _Locations.SetPlace(place);
                latitude = _Locations.GetLatitude();
                longitude = _Locations.GetLongitude();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.d("Error", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                Log.d("Error", "Cancelado");
                // The user canceled the operation.
            }
        }
    }

    public void CreateMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void PlaceResult(Place place) {
        _Locations.SetPlace(place);
        GetPositionMap();
    }


    public void onMarkerDrag(Marker marker){

    }

    public void onMarkerDragEnd (Marker marker){
        _Locations.SetMarker(marker);
        GetPositionMap();
    }

    public void onMarkerDragStart (Marker marker){

    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        _Locations.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", String.valueOf(location));

    }

    private void SetDistance(int distance){
        _Locations.distance = distance;
    }

    private void UpdateStorage(){
        try {
            JSONObject user = new JSONObject();
            user.put("latitude", String.valueOf(_Locations.latitude));
            user.put("longitude", String.valueOf(_Locations.longitude));
            user.put("distance", String.valueOf(_Locations.distance));
            ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                @Override
                public void processFinish(String output){
                    //Toast.makeText(mContext, output, Toast.LENGTH_SHORT).show();
                    //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                }
            };

            new ClassDataStorage(mContext, "write", "locations", user.toString(), Callback).execute("");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onInfoWindowClick(Marker marker) {
        onMarkerClick(marker);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        //marker.hideInfoWindow();
        return false;
    }

    public void MapsInfoOptionRedirect(Marker marker){
        //String id = markerIds.get(marker);
        String id = marker.getSnippet();
        //Log.d("onMarkerClick",String.valueOf(id));
        if(id!="" && id!=null){
            _Places.PlaceSelect = id;
            Intent intent = new Intent(this, ActivityPlace.class);
            intent.putExtra("option",id);
            startActivity(intent);
        }
    }

}