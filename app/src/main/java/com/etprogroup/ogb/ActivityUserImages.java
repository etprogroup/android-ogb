package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class ActivityUserImages extends ActivityRoot{

    private Activity mContext;
    private Class_User _User;
    private String token;
    private JSONObject Images = new JSONObject();

    @Override
    public int getLayoutResource() {
        return R.layout.activity_userimages;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_userimages);
        mContext = this;
        _User = ((App) getApplicationContext()).User();
        token = _User.GetDataToken();

        //FABS
        configureFabAddImage();

        //IMAGES
        fillImages();

        //Tolbar
        if(toolbar != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    private void fillImages(){
        final JSONObject data = new JSONObject();
        try {
            data.put("action", "user_images");
            data.put("token", token);
            data.put("user", _User.GetDataUser());
            data.put("option", "get");
            data.put("path", "uploads/profile/"+token);

        } catch (JSONException e){
            e.printStackTrace();
        }


        ClassDataServer.AsyncResponse UserResponse = new ClassDataServer.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                //new ClassGeneral().CreateMenssage(mContext,"Update", output);
                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("success")){
                        //new ClassGeneral().CreateMenssage(mContext,"Update", message);

                        if(!response.isNull("images")){
                            Images = new JSONObject(response.getString("images"));
                            showImages(Images);
                        }

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);
                    }

                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };
        new ClassDataServer(mContext, data, UserResponse).execute("");
    }


    private void showImages(JSONObject images){
        GridView gridView = (GridView) findViewById(R.id.GridViewImages);
        ClassGridviewProfiles gridViewAdapter = new ClassGridviewProfiles(this, images);
        gridView.setAdapter(gridViewAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CharSequence idProfile = ((TextView) view.findViewById(R.id.gridview_id)).getText();
                CharSequence name = ((TextView) view.findViewById(R.id.gridview_text)).getText();
                //Toast.makeText(getApplicationContext(), name, Toast.LENGTH_SHORT).show();

                /*
                Intent intent = new Intent(mContext, ActivityProfile.class);
                intent.putExtra("option",idProfile);
                mContext.startActivity(intent);
                */
            }
        });

    }

    //FABS
    private void configureFabAddImage() {
        //Floating Action Button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.FABaddImage);
        if(fab != null){
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddImage();
                }
            });
        }
    }





    //UPLOAD IMAGE
    private ClassDataServerFile DataServerFile;
    private static final int CAPTURE_IMAGE_ACTIVITY = 1;
    private Uri mImageCaptureUri;

    public void AddImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), CAPTURE_IMAGE_ACTIVITY);
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CAPTURE_IMAGE_ACTIVITY) {
            //TODO: action
            if (data == null) {
                //Display an error
                return;
            }

            Uri URIimage = data.getData();
            String Image = new ClassGeneral(mContext).getPathbyURI(mContext, URIimage);

            if(Image != null){
                File FileImage = new File(Image);

                /*
                ArrayList<String>  ListImage = new ClassGeneral(mContext).getRealPathFromURI(mContext, URIimage);
                String Image = ListImage.get(0);
                Log.d("ListImage", String.valueOf(ListImage));
                Log.d("Image",  String.valueOf(Image));
                */

                if(FileImage.exists()){
                    AddImageServer(Image, URIimage);

                }else{
                    new ClassGeneral().CreateMenssage(mContext,"Error", "Error Image Selected");

                }
            }

        }
    }


    public void AddImageServer(final String path, final Uri URIimage){
        final JSONObject data = new JSONObject();

        try {
            data.put("action", "image");
            data.put("token", token);
            data.put("user", _User.GetDataUser());
            data.put("path", "uploads/profile/"+token);
            data.put("name_change", false);
            data.put("image_name", "file");
            data.put("image", path);

        } catch (JSONException e){
            e.printStackTrace();
        }

        final ClassDataServerFile.AsyncResponse FileResponse = new ClassDataServerFile.AsyncResponse(){

            @Override
            public void processFinish(String output){

                try {
                    JSONObject response = new JSONObject(output);
                    String result = response.getString("result");
                    String message = response.getString("message");

                    if(result.equals("true")){
                        final String file = response.getString("file");
                        ClassImageDownload.Cache.getInstance().getLru().remove(new ClassGeneral().CreateURL(mContext,file));
                        //_User.SetDataImage(file);
                        //ProfileImage.setImageURI(URIimage);
                        //EditImage.setText(file);
                        fillImages();

                    }else if(result.equals("false")){
                        new ClassGeneral().CreateMenssage(mContext,"Error", message);

                    }else if(result.equals("success")){

                    }else if(result.equals("etoken")){
                        Intent Intent = new Intent(mContext, ActivityMain.class);
                        startActivity(Intent);

                    }else{
                        new ClassGeneral().CreateMenssage(mContext,"Result", output);

                    }


                } catch (JSONException e) {
                    new ClassGeneral().CreateMenssage(mContext,"Error", output);
                    e.printStackTrace();
                }

            }
        };

        DataServerFile = new ClassDataServerFile(mContext, data, path, FileResponse);
        DataServerFile.execute("");
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if(DataServerFile != null){
            DataServerFile.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}