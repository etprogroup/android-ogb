package com.etprogroup.ogb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Gomez on 17-01-2018.
 */

public class ActivitySettings extends ActivityRoot {
    private Activity mContext;
    private Switch SwitchNotify;
    private Switch SwitchSound;
    private Switch SwitchVibration;
    private SeekBar SeekBarDistance;
    private TextView TextViewDistanceUnit;
    private int Distance = 10;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_settings);
        mContext = this;

        SeekBarDistance = (SeekBar) findViewById(R.id.SeekBarDistance);
        TextViewDistanceUnit = (TextView) findViewById(R.id.TextDistanceUnit);
        SwitchVibration = (Switch) findViewById(R.id.SwitchVibration);
        SwitchSound = (Switch) findViewById(R.id.SwitchSound);
        SwitchNotify = (Switch) findViewById(R.id.SwitchNotify);


        //LOAD
        ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

            @Override
            public void processFinish(String output){
                //new ClassGeneral().CreateMenssage(mContext,"GET", output);

                try {
                    JSONObject Callback = new JSONObject(output);

                    if(!Callback.isNull("distance")){
                        Distance = Integer.parseInt(Callback.getString("distance"));
                    }

                    if(!Callback.isNull("sound")){
                        SwitchSound.setChecked(Boolean.parseBoolean(Callback.getString("sound")));
                    }

                    if(!Callback.isNull("vibration")){
                        SwitchVibration.setChecked(Boolean.parseBoolean(Callback.getString("vibration")));
                    }

                    if(!Callback.isNull("notify")){
                        SwitchNotify.setChecked(Boolean.parseBoolean(Callback.getString("notify")));
                    }



                    SettingsDistance();
                    SettingsNotify();
                    SettingsVibration();
                    SettingsSound();


                }catch(JSONException e) {
                    e.printStackTrace();
                }

            }
        };

        new ClassDataStorage(mContext, "read", "settings", "", Callback).execute("");

    }

    public void SettingsDistance(){
        final String DistanceUnit = getString(R.string.ActivitySettingsDistanceUnit);

        SeekBarDistance.setProgress(Distance);
        TextViewDistanceUnit.setText(String.valueOf(Distance)+" "+DistanceUnit);
        SeekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int progress = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                TextViewDistanceUnit.setText(String.valueOf(Distance)+" "+DistanceUnit);
                Distance = progresValue;
                progress = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                TextViewDistanceUnit.setText(String.valueOf(Distance)+" "+DistanceUnit);
                UpdateData();
            }
        });
    }

    public void SettingsSound(){
        SwitchSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UpdateData();
            }
        });
    }

    public void SettingsVibration(){
        SwitchVibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UpdateData();
            }
        });
    }

    public void SettingsNotify(){
        SwitchNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                UpdateData();
            }
        });
    }

    public void UpdateData(){

        try {
            JSONObject data = new JSONObject();
            data.put("distance", Distance);
            data.put("notify", SwitchNotify.isChecked());
            data.put("vibration", SwitchVibration.isChecked());
            data.put("sound", SwitchSound.isChecked());

            ClassDataStorage.AsyncResponse Callback = new ClassDataStorage.AsyncResponse(){

                @Override
                public void processFinish(String output){
                    //new ClassGeneral().CreateMenssage(mContext,"SAVE", output);
                }
            };

            new ClassDataStorage(mContext, "write", "settings", data.toString(), Callback).execute("");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
